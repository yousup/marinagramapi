class AnimalController < ApplicationController
  	before_filter :restrict_access
  	def show
      @animal = {0 => Animal.getDailyAnimal()}
  		respond_to do |format|
	      format.json {render json: @animal}
	    end
  	end

    def index
      if params[:flag] == "aquarium"
        @animals = Animal.getAllForAquarium(params[:context].titleize)
        @return_json = {}
        counter = 0
        @animals.each do |a|
          @return_json[counter] = a
          counter += 1
        end
      end
      respond_to do |format|
        format.json {render :json => @return_json.to_json}
      end
    end

	private
	
  	def restrict_access
  		authenticate_or_request_with_http_token do |token, options|
   			ApiKey.exists?(access_token: token)
  		end
	end
end
