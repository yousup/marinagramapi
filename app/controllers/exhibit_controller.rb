class ExhibitController < ApplicationController
  	before_filter :restrict_access
    def index
      @exhibits = Exhibit.getAllForAquarium(params[:aquarium].titleize)
      @return_json = {}
      counter = 0
      @exhibits.each do |e|
        @return_json[counter] = e
        counter += 1
      end
      respond_to do |format|
        format.json {render json: @return_json}
      end
    end

  	def show
  		respond_to do |format|
	      format.json
	    end
  	end

	private
	
  	def restrict_access
  		authenticate_or_request_with_http_token do |token, options|
   			ApiKey.exists?(access_token: token)
  		end
	end
end
