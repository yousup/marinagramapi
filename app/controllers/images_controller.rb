class ImagesController < ApplicationController
	Mime::Type.register "image/jpg", :jpg
	before_filter :restrict_access

	def show
		@image = params[:path].downcase.gsub("_","")+".jpg"
		respond_to do |format|
            format.jpg {send_file 'app/assets/images/'+@image, type: 'image/jpeg', disposition: 'inline'}
        end
	end

	def thumbnail 
		@image = params[:path].downcase.gsub("_","")+".jpg"
		respond_to do |format|
			format.jpg {send_file 'app/assets/images/thumbnails/'+@image, type: 'image/jpeg', disposition: 'inline'}
		end
	end

	def showpost
		@image = params[:path].downcase.gsub("_","")+".jpg"
		respond_to do |format|
            format.jpg {send_file 'app/assets/images/Posts/'+@image, type: 'image/jpeg', disposition: 'inline'}
        end
	end

	def restrict_access
  		authenticate_or_request_with_http_token do |token, options|
   			ApiKey.exists?(access_token: token)
  		end
	end
end
