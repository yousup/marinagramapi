class AquariumController < ApplicationController
	before_filter :restrict_access
  	def show
  		@aquarium = {0 => Aquarium.find_by_name(params[:name].titleize)}
  		respond_to do |format|
	      format.json { render json: @aquarium }
	    end
  	end

  	def index
  		@resultJSON = {}
  		counter = 0
  		Aquarium.all.order(:name).each do |a|
  			@resultJSON[counter] = a
  			counter += 1
  		end
  		respond_to do |format|
  			format.json {render json: @resultJSON}
  		end
  	end

	private

  	def restrict_access
  		authenticate_or_request_with_http_token do |token, options|
   			ApiKey.exists?(access_token: token)
  		end
	end
end
