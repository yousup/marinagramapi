class PostController < ApplicationController
  def create
  end

  def show
  	if params[:id] == "0"
  		@posts = Post.retrievePosts()
  	else
  		@posts = Post.retrieveNextPosts(params[:id])
  	end
  	@return_json = {}
    counter = 0
    @posts.each do |p|
      @return_json[counter] = p
      counter += 1
    end
  	respond_to do |format|
        format.json {render :json => @return_json.to_json}
    end
  end

  def count
    @postcount = {"count" => Post.count}
    respond_to do |format|
      format.json {render :json => @postcount.to_json}
    end
  end
end
