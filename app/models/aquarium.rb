class Aquarium < ActiveRecord::Base
	validates :name, presence: true
	validates :location, presence: true
	# validates :about, presence: true
	validates :image, presence: true	
end
