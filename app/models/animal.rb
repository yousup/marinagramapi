class Animal < ActiveRecord::Base
	class << self; attr_accessor :daily end
	@daily = 1

	validates :name, presence: :true
	validates :scientificName, presence: :true
	validates :image, presence: :true
	validates :tidbit, presence: :true
	validates :aquarium, presence: true

	def Animal.getAllForAquarium(aquariumName)
		# aquarium = Aquarium.find_by_name(aquariumName)
		logger.info aquariumName
		return Animal.where('aquarium = ?', aquariumName).order(:name)
	end

	def Animal.changeDailyAnimal()
		if @daily == 0
			@daily = 1
		else
			newDaily = rand(1..Animal.size)
			while newDaily == @daily
				newDaily = rand(1..Animal.size)
			end
			@daily = newDaily
		end
	end

	def Animal.getDailyAnimal()
		Animal.find(@daily)
	end
end

