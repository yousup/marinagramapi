class Post < ActiveRecord::Base
	validates :title, presence: :true
	validates :content, presence: :true
	validates :date, presence: :true
	validates :image, presence: :true
	validates_format_of :date, with: /\A(\d{1,2})\/(\d{1,2})\/(\d{4})\z/, on: :create

	def Post.retrievePosts()
		posts = []
		a = Post.last
		posts.append(a)
		b = Post.where("id < ?", a.id).last
		if b != nil
			posts.append(b)
			c = Post.where("id < ?", b.id).last
			if c != nil
				posts.append(c)
			end
		end

		return posts
	end

	# Gets up to the next three posts
	def Post.retrieveNextPosts(lastPostID)
		posts = []
		if lastPostID.to_i == 0 or Post.find(lastPostID.to_i) != Post.first
			a = Post.where("id < ?", lastPostID.to_i).last
			posts.append(a)
			if a!=nil and Post.exists?(id: a.id) and Post.find(a.id) != Post.first
				b = Post.where("id < ?", a.id).last
				posts.append(b)
				if b!=nil and Post.exists?(id: b.id) and Post.find(b.id) != Post.first
					posts.append(Post.where("id < ?", b.id).last)
				end
			end
		end

		return posts
	end 
end
