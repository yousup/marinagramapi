class CreateAquaria < ActiveRecord::Migration
  def change
    create_table :aquaria do |t|
      t.string :name
      t.string :image
      t.string :about
      t.string :location

      t.timestamps
    end
  end
end
