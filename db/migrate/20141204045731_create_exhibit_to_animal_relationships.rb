class CreateExhibitToAnimalRelationships < ActiveRecord::Migration
  def change
    create_table :exhibit_to_animal_relationships do |t|
      t.integer :exhibit_id
      t.integer :animal_id

      t.timestamps
    end
  end
end
