class CreateAnimals < ActiveRecord::Migration
  def change
    create_table :animals do |t|
      t.string :name
      t.string :scientificName
      t.string :image
      t.string :tidbit
      t.string :diet
      t.string :length
      t.string :weight

      t.timestamps
    end
  end
end
