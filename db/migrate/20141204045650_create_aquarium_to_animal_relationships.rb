class CreateAquariumToAnimalRelationships < ActiveRecord::Migration
  def change
    create_table :aquarium_to_animal_relationships do |t|
      t.integer :aquarium_id
      t.integer :animal_id

      t.timestamps
    end
  end
end
