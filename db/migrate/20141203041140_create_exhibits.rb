class CreateExhibits < ActiveRecord::Migration
  def change
    create_table :exhibits do |t|
      t.string :name
      t.string :image
      t.string :about

      t.timestamps
    end
  end
end
