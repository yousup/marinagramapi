class AddAquariumToExhibit < ActiveRecord::Migration
  def change
    add_reference :exhibits, :aquarium, index: true
    add_index :exhibits, [:aquarium_id, :created_at]
  end
end
