Aquide::Application.routes.draw do
  get "posts/create"
  get "posts/show"
  get "images/show"
  get "aquarium/show"
  get "exhibit/show"
  get "animal/show"

  match '/api/aquarium/all', to: 'aquarium#index', via: "get"
  match '/api/exhibit/:aquarium/', to: 'exhibit#index', via: "get"
  match '/api/animal/:context/:flag/', to: 'animal#index', via: "get"
  match '/api/animal/daily', to: 'animal#show', via: "get"
  match '/api/aquarium/:name', to: 'aquarium#show', via: "get"
  match '/api/exhibit/:name', to: 'exhibit#show', via: "get"
  match '/api/animal/:name', to: 'animal#show', via: "get"
  match '/api/post/postcount', to: 'post#count', via: "get"
  match '/api/post/:id', to: 'post#show', via: "get"

  match 'thumbnailimage/animal/:path', to: 'images#thumbnail', via: "get"
  match 'image/aquarium/:path', to: 'images#show', via: "get"
  match 'image/exhibit/:path', to: 'images#show', via: "get"
  match 'image/animal/:path', to: 'images#show', via: "get"
  match 'image/post/:path', to: 'images#show', via: "get"
  match 'image/post/Posts/:path', to: 'images#showpost', via: "get"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
